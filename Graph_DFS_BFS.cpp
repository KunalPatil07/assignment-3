#include <iostream>
using namespace std;

struct node
{
	int vertex;
	struct node *next;
};

class Q
{
	int data[10];
	int F,R;
	public:
	Q()
	{
		F=R=-1;
	}
	int isEmpty()
	{
		if(R==-1)return 1;
		return 0;
	}
	void enqueue(int i)
	{
		data[++R]=i;
	}
	int dequeue()
	{
		int i=data[++F];
		if(F==R)
			F=R=-1;
		return i;
	}
};

//====================================================

class Graph
{
 int n,no,G[10][10],visited[10];
 struct node *G1[10];
 int visited1[10];
 public:
 void insertEdge(int,int);
 Graph()
 {
	 n=no=5;
	 for(int j=0;j<10;j++)
		 visited[j]=0;
	 for(int j=0;j<10;j++)
	 {
		 G1[j]=NULL;
		 visited1[j]=0;
	 }

 }

 
 void displayList();
 
 void DFS_List(int);
 
 void BFS_List(int);
};






//====================================================
void Graph::insertEdge(int Vi,int Vj)
{
	struct node *T;
	struct node *newNode=new node;
	newNode->vertex=Vj;
	newNode->next=NULL;
	if(G1[Vi]==NULL)
		G1[Vi]=newNode;
	else
	{
		T=G1[Vi];
		while(T->next!=NULL)
			T=T->next;
		T->next=newNode;
	}
}
//====================================================
void Graph::displayList()
{
	int i;
	struct node *T;
	cout<<"\nThe Adjacency List representation of graph is:";
	for(i=0;i<no;i++)
	{
		cout<<"\n";
		cout<<i<<"-> {";
		T=G1[i];
		while(T!=NULL)
		{
			cout<<T->vertex<<"  ";
			T=T->next;
		}
		cout<<" }";
	}
}



//====================================================
void Graph::DFS_List(int i)
{
	struct node *T;
	T=G1[i];
	cout<<i<<"  -  ";
	visited1[i]=1;
    while(T!=NULL)
    {
    	i=T->vertex;
    	if(!visited1[i])
    		DFS_List(i);
    	T=T->next;
    }
}
//====================================================

//====================================================
void Graph::BFS_List(int i)
{
	Q q;
	struct node *T;
	for(int m=0;m<no;m++)
		visited1[m]=0;
	q.enqueue(i);
	cout<<i<<"  -  ";
	visited1[i]=1;
	while(!q.isEmpty())
    {
    	i=q.dequeue();
    	T=G1[i];
    	while(T!=NULL)
    	{
    		i=T->vertex;
    		if(visited1[i]==0)
    		{
    			q.enqueue(i);
    			visited1[i]=1;
    			cout<<i<<"  -  ";
    		}
    		T=T->next;
    	}
    }
}
//====================================================
int main() {
	int ch;
	Graph g;
	g.insertEdge(0,1);
	g.insertEdge(0,3);
	g.insertEdge(1,2);
	g.insertEdge(2,3);
	g.insertEdge(3,4);
	g.displayList();
	cout<<"\n------------------------DFS---------------------------\n";
	g.DFS_List(0);
	cout<<"\n------------------------BFS---------------------------\n";
	g.BFS_List(0);
	return 1;
}
